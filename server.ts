import app from './src/app'
import ServerConfig from './src/config/ServerConfig'


app.listen(ServerConfig.port, () => {
    console.log(`Server is running on port ${ServerConfig.port}`)
})

