# Esqueleto Backend

El repositorio esta creado con el fin de comenzar a desarrollar un BackEnd en nodejs sin partir de 0. La idea es clonarlo y a partir de la estructura básica seguir ampliando funcionalidades.


## Estructura del proyecto

La estructura/arquitectura del proycto es la siguiente:

![estructura del proyecto](structure.svg)

1. `routes/`: definición de los endpoints de la API e implementación del controlador correspondiente para cada ruta.
1. `controllers/`: lógica vinculada a las rutas, se transforman los datos provenientes del usuario y enviados al mismo; se utilizan los use-cases.
1. `use-cases/`: lógica que debe ser independiente de las rutas, solo serán utilzados por los controladores, no conocerán lo que es un objeto request o response *(esto lo saben los controladores)*. Se usan para trabajar por ejemplo con la base de datos.
1. `utils/`: utilidades que se pueden utilizar en cualquier parte.
1. `middleware/`: los métodos que se ejecutan antes de la logica vincualada a las rutas. Pueden ser globales (para toda la app) o para rutas concretas.
1. `models/`: la definición de las tablas de la base de datos si utilizamos un ORM.
1. `data-access/`: las consultas para la base de datos utilizando los models definidos. El acceso a datos se utilizará en los use-cases


## Tecnologias

- `Typescript`: como lenguaje
- `Jest` y `Supertest`: para el testing *(unitarios y de integración)*
- `ExpressJS`: como framework web
- `Argon2`: para manejo de contraseñas encriptadas
- `JWT`: para manejar tokens de usuarios *(sesiones, roles, etc)*
- `Sequelize`: como ORM para diferentes tipos de bases de datos *(Sql & NoSql)*

