import * as argon2 from 'argon2'

export class Authentication {
    static async generatePassword(plainPassword: string): Promise<string> {
        try {
            const options = {
                timeCost: 4,       // Número de iteraciones (tiempo)
                memoryCost: 2 ** 16, // Cantidad de memoria en KiB (memoria)
                parallelism: 2,   // Número de hilos/paralelización
                saltLength: 32,   // Longitud del salt en bytes
                hashLength: 64    // Longitud del hash en bytes
            }

            const hash = await argon2.hash(plainPassword, options)
            return hash
        } catch (error) {
            if (error instanceof Error) {
                console.error('Error generating password hash:', error)
                return error.message
            }
            return "Error generating password hash"

        }
    }

    static async comparePassword(plainPassword: string, hashPassword: string): Promise<boolean> {
        try {
            return await argon2.verify(hashPassword, plainPassword)
        } catch (error) {
            return false
        }

    }
}