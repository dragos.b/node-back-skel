export interface IUserRole {
    level: number
    name: string
}

const UserRoles: Record<string, IUserRole> = {
    SUPER: { level: 1, name: "super" },
    ADMIN: { level: 2, name: "admin" },
    CLIENT: { level: 3, name: "client" },
    DISABLED: { level: 4, name: "disabled" },
}


export default UserRoles