import jwt from "jsonwebtoken"
import { Request, Response, NextFunction } from 'express';
import { StatusCodes } from "http-status-codes";
import authConfig from "../../config/AuthConfig";
import { DecodedToken } from "./IDecodedToken";
import { CustomError } from "../CustomError";

export class TokenService {
    static getTokenFromBearer(tokenString: string | undefined): string {
        if (tokenString === undefined) {
            return '';
        }
        return tokenString.split(' ')[1];
    }

    static decodeToken(token: string): DecodedToken | undefined {
        try {
            return jwt.verify(
                token,
                authConfig.secret,
                { algorithms: [authConfig.algorithm as jwt.Algorithm] }
            ) as unknown as DecodedToken;
        } catch {
            throw new CustomError('Invalid token', StatusCodes.UNAUTHORIZED)
        }

    }

    static generateToken(data: DecodedToken): string {
        const token = jwt.sign(
            data,
            authConfig.secret,
            { "algorithm": authConfig.algorithm as jwt.Algorithm });

        return `Bearer ${token}`;
    }

    static getUserTypeFromToken(token: string): number | undefined {
        if (this.isValidJwtStructure(token)) {
            const string_token = this.getTokenFromBearer(token);
            const decoded_token = this.decodeToken(string_token);
            return decoded_token?.user_type;
        }
    }

    static isValidTokenMiddle(req: Request, res: Response, next: NextFunction): Response | void {
        const tokenString = req.get('authorization');

        if (!tokenString) {
            return res
                .status(StatusCodes.UNAUTHORIZED)
                .json({ error: 'Auth token missing' });
        }
        if (this.isValidJwtStructure(tokenString)) {
            next();
        }
        else {
            res
                .status(StatusCodes.UNAUTHORIZED)
                .json({ error: 'Invalid auth token structure' });
        }
    }

    static decodeDataFromToken(token: string): DecodedToken | undefined {
        if (this.isValidJwtStructure(token)) {
            const string_token = this.getTokenFromBearer(token);
            const decoded_token = this.decodeToken(string_token);
            return decoded_token;
        }
    }

    static isValidJwtStructure(tokenString: string | undefined): boolean {
        const parts = tokenString?.split('.');
        return parts?.length === 3;
    }
}

