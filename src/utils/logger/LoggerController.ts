import { pino } from "pino"
import { pinoHttp, HttpLogger } from "pino-http"
import { RotatingFileStream, createStream } from "rotating-file-stream"
import { mkdirSync } from "fs"
import * as fs from "fs"
import path from "path"
import LoggerConfig from "../../config/LoggerConfig"

/**
 * Controller for managing log operations.
 * This controller uses the Singleton design pattern to ensure there's only one instance of the logger.
 *
 * Work like middleware in express for http logging and basic logger
 */
export class LoggerController {
    private static instance: LoggerController
    private filePath!: string
    private stream!: RotatingFileStream
    private isConfigured: boolean = false

    private static logger: pino.Logger

    /**
     * Private constructor to prevent direct construction calls with the `new` operator.
     * Initializes the logger's configuration.
     */
    private constructor() {
        try {
            this.filePath = path.join(process.cwd(), LoggerConfig.directoryName)
            console.log(`PATH: ${this.filePath}`)

            this.checkDirectory()
            this.stream = createStream(LoggerConfig.outputLogFileName, {
                interval: LoggerConfig.rotationIntervalTime,
                path: this.filePath,
            })
            LoggerController.logger = pino(this.stream)


            this.isConfigured = true
        } catch (err) {
            console.log(err)
            this.isConfigured = false
        }

    }

    /**
     * The static method that controls the access to the LoggerController instance.
     * @returns The instance of the LoggerController.
     */
    static getInstance() {
        if (!LoggerController.instance) {
            LoggerController.instance = new LoggerController()
        }
        return LoggerController.instance
    }

    /**
     * Returns an HttpLogger (from pino-http) if the logger is configured.
     * @returns HttpLogger | null
     */
    getHttpLogger(): HttpLogger | null {
        if (this.isConfigured) {
            return pinoHttp({ logger: LoggerController.logger })
        } else {
            return null
        }
    }

    /**
     * Logs a message with the specified log level.
     * @param log - The LoggerModel object that contains the level and message to be logged.
     */
    log(log: LoggerModel) {

        try {
            this.assertIsValidLoggerModel(log)

            const message = {
                level: log.level,
                message: log.message
            }

            switch (log.level) {
                case "debug":
                    LoggerController.logger.debug(message)
                    break
                case "info":
                    LoggerController.logger.info(message)
                    break
                case "warn":
                    LoggerController.logger.warn(message)
                    break
                case "error":
                    LoggerController.logger.error(message)
                    break
                default:
                    LoggerController.logger.warn(message)
                    break
            }
        } catch (err) {
            console.log(err)
        }

    }

    /**
     * Checks if the log directory exists, and creates it if it doesn't.
     * Throws an error if the directory couldn't be created.
     */
    private checkDirectory() {
        try {
            if (!fs.existsSync(this.filePath))
                mkdirSync(this.filePath)
        } catch (err) {
            console.log("El directorio no se ha creado")
            console.log(err)
            throw new Error("El directorio no se ha creado")
        }
    }

    /**
     * Checks if the object is a LoggerModel. This method is used in execution
     * time in Javascript because JS doesn't support type checking.
     */
    private assertIsValidLoggerModel(object: any) {
        if (!object) {
            throw new Error("LoggerModel object is null or undefined.")
        }
        if (typeof object.message !== 'string') {
            throw new Error("LoggerModel object is missing a string message property.")
        }
        if (!["debug", "info", "warn", "error"].includes(object.level)) {
            throw new Error("LoggerModel object is missing a valid level property.")
        }
    }

}