type Level = "debug" | "info" | "warn" | "error"

interface LoggerModel {
    message: string,
    level: Level
}