// import { UserModel } from "../models/UserModel"
import { TokenService } from "../utils/token/TokenService"
import { DecodedToken } from "../utils/token/IDecodedToken"

export class UsersUseCase {
    // In this class use UserDataAccess object to CRUD operations with DB.
    // And use an instance of this class in UserController.

    // Create user in database
    // Get all users from db
    // Update user
    // etc

    // Tener un atributo del usuario para evitar consultas todo el rato.
    static login(username: string, password: string): boolean {
        console.log(username, password)
        // Verificar si existe el usuario en la base de datos
        // Si es correcto generar un JWT
        this.generateJWT()
        return true
    }

    private static generateJWT(): string {
        const d : DecodedToken = {
            user_type: 1
        }

        TokenService.generateToken(d)
        return 'JWT'
    }
}