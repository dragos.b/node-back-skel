import express from 'express'
import helmet from 'helmet'

import serverConfig from './config/ServerConfig'
import { LoggerController } from './utils/logger/LoggerController'
import { HttpLogger } from 'pino-http'
import { userRouter } from './routes/UserRouter'
import ErrorMiddleware from './middleware/ErrorMiddleware'

/** Main Application */
const app = express()
const logger = LoggerController.getInstance()
const httpLogger: HttpLogger = logger.getHttpLogger()!


/** Middlewares for Application */
app.use(express.json())
app.use(helmet(
    serverConfig.helmetOptions
))

app.use(httpLogger)
/** Routes */
app.use(serverConfig.userRoute, userRouter)

/** Error Handler Middleware */
app.use(ErrorMiddleware.errorResponse())


export default app
