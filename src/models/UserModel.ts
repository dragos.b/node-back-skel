// Define the ORM class. In this case sequelize
// https://sequelize.org/docs/v6/core-concepts/model-basics/

import { Sequelize, DataTypes } from "sequelize"
const sequelize = new Sequelize('sqlite::memory:')

export const UserModel = sequelize.define('User', {
    // Model attributes are defined here
    firstName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    lastName: {
        type: DataTypes.STRING
    }
}, {
});

UserModel.init({
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    }
}, { sequelize })