import express from 'express'
import { Request, Response } from 'express'
import UserAuthority from '../middleware/auth/UserAuthority'
import UserRoles from '../utils/UserRoles'
import { UsersController } from '../controllers/UsersController'

import { LoggerController } from '../utils/logger/LoggerController'

export const userRouter = express.Router()

// All routes for 'user' API
// Use the controller in routes.

userRouter.get('/', (_req: Request, res: Response) => {
  console.log('_req.headers')
  res.send('All users')
})

userRouter.get(
  '/:id',
  UserAuthority.permitOnly([UserRoles.SUPER, UserRoles.ADMIN]),
  (req: Request, res: Response) => {
    res.send(`User with id ${req.params.id}`)
  })

userRouter.post(
  '/',
  UserAuthority.permitOnly([UserRoles.SUPER]),
  (_req: Request, res: Response) => {
    res.send('Create user')
  })

userRouter.post(
  '/login',
  (req: Request, res: Response) => {
    LoggerController.getInstance().log({ level: "info", message: `Login user` })
    const result = UsersController.login(req, res)
    res.json(result)
  }
)

userRouter.put('/', (_req: Request, res: Response) => {
  res.send('Update user')
})

userRouter.delete('/', (_req: Request, res: Response) => {
  res.send('Delete user')
})