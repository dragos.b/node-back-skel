import { Request, Response } from 'express'
import { UsersUseCase } from '../use-cases/UsersUseCase'


export class UsersController {
    // Get request data and parse the data and use the UseCase
    static login(req: Request, _res: Response) {
        const { username, password } = req.body
        UsersUseCase.login(username, password)

        return "JWT"

    }
}