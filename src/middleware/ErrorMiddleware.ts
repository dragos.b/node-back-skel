import { ErrorRequestHandler, NextFunction, Request, Response } from "express"
import { StatusCodes } from "http-status-codes"
import { CustomError } from "../utils/CustomError"

export default class ErrorMiddleware {
    static errorResponse(): ErrorRequestHandler {
        return (
            error: Error | CustomError,
            _req: Request,
            res: Response,
            _next: NextFunction
        ): Response => {
            if (error instanceof CustomError) {
                return res
                    .status(error.statusCode)
                    .json({ error: error.message })
            } else {
                return res
                    .status(StatusCodes.INTERNAL_SERVER_ERROR)
                    .json({ error: `${error}` })
            }
        }
    }
}