import { Response, NextFunction, RequestHandler } from "express"
import { StatusCodes } from "http-status-codes"
import UserRoles, { IUserRole } from "../../utils/UserRoles"
import { TokenService } from "../../utils/token/TokenService"
import { CustomError } from "../../utils/CustomError"


/**
 * Clase para la autorización y autenticación de los usuarios
 * @export
 * @class UserAuthority
 */
export default class UserAuthority {
    /**
     * Middleware que permite la entrada solo a los roles especificados
     *
     * Primero valida y decodifica el token JWT, luego verifica si el tipo de usuario se encuentra en el array de roles permitidos
     *
     * @static
     * @param {IUserRole[]} roles - Los roles de usuario que tienen permiso
     * @returns {RequestHandler[]} Un array de middlewares a ser utilizado en la ruta
     */
    static permitOnly = (roles: IUserRole[]): RequestHandler[] => {
        return [
            UserAuthority.validateAndDecodeToken,
            (req: any, _res: Response, next: NextFunction): Response | void => {
                console.log(req.user.user_type)
                if (req.user.user_type === UserRoles.DISABLED.level) {
                    throw new CustomError('Disabled user', StatusCodes.FORBIDDEN)
                }

                const userTypesArray = roles.map(role => {
                    return role.level
                })

                console.log(`El array es: ${userTypesArray}`)

                if (!userTypesArray.includes(req.user.user_type)) {
                    throw new CustomError('You don\'t have permission', StatusCodes.FORBIDDEN)
                }

                next()
            },
        ]
    }

    /**
 * Middleware privado para validar y decodificar el token JWT
 *
 * Obtiene el token de la cabecera 'Authorization', valida su estructura, luego decodifica y asigna el contenido decodificado al objeto req.user.
 *
 * @private
 * @static
 * @param {Request} req - La solicitud HTTP
 * @param {Response} _res - La respuesta HTTP
 * @param {NextFunction} next - La función next() de Express para pasar al siguiente middleware
 * @returns {RequestHandler} Middleware que valida y decodifica el token JWT
 */
    private static validateAndDecodeToken: RequestHandler =
        (req: any, _res: Response, next: NextFunction) => {
            const auth = req.get('authorization')
            if (!TokenService.isValidJwtStructure(auth)) {
                throw new CustomError('Invalid auth token structure', StatusCodes.UNAUTHORIZED)
            }
            const token = TokenService.getTokenFromBearer(auth)
            try {
                const decodedToken = TokenService.decodeToken(token)
                if (!decodedToken) {
                    throw new CustomError('Auth token missing', StatusCodes.UNAUTHORIZED)
                }
                req.user = decodedToken
                next()
            } catch (error: CustomError | any) {
                next(error)
            }
        };
}