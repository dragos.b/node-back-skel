// To make integration tests, use Supertest with Jest.

import request, { Response } from 'supertest'

import app from '../../../src/app'
import ServerConfig from '../../config/ServerConfig'

describe('Users', () => {

    describe('GET /users', () => {

        it('should get all users', async () => {
            const res: Response = await request(app)
                .get(ServerConfig.userRoute)
            expect(res.statusCode).toBe(200)


        })

    })



})