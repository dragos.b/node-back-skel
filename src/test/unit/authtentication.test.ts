import { Authentication } from "../../utils/Authentication"

describe("UsersUseCase", () => {
    const encryptedpwd = "$argon2id$v=19$m=65536,t=4,p=2$eLL3+nnlUezZoW1BoWOwDuboq4IGG6f2/J833O8zh9M$S3v/rFEwEjaLp3X5AS8dIi62ldYlDSilscK4OKL2e6KO0B2RlsJwO3P+xWE88ORX7k4YXUR2JfAoa39IHbBi6Q"


    it("should be generate password", () => {
        const pwd = Authentication.generatePassword("password")
        console.log(pwd)
        expect(pwd)
    })

    it("generate different passwords", async () => {
        const pwdOne = await Authentication.generatePassword("password")
        const pwdTwo = await Authentication.generatePassword("password")

        console.log(`One: ${pwdOne}`)
        console.log(`Two: ${pwdTwo}`)

        expect(pwdOne).not.toEqual(pwdTwo)
    })

    it("compare passwords", async () => {
        const isSame = await Authentication.comparePassword("password", encryptedpwd)
        console.log(`Is same password: ${isSame}`)
        expect(isSame).toEqual(true)
    })

    it("should be return false on compare password", async () => {
        const isSame = await Authentication.comparePassword("bad-password", encryptedpwd)
        console.log(`Is same password: ${isSame}`)
        expect(isSame).toEqual(false)
    })
})
