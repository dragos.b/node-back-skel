export default {
    port: Number(process.env.PORT) || 3000,
    userRoute: process.env.USER_ROUTE || "/user",
    helmetOptions: {
        crossOriginResourcePolicy: false,

    }
}
