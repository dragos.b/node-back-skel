export default {
    secret: process.env.JWT_SECRET || 'SECRET',
    algorithm: 'HS512',
}