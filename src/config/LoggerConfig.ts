export default {
    rotationIntervalTime: process.env.ROTATION_INTERVAL_TIME || '1d', //TODO: ver la doc
    // Relative path from cwd() of server.js(ts)
    directoryName: process.env.DIRECTORY_NAME || 'logs',
    outputLogFileName: process.env.OUTPUT_LOG_FILE || '.log',
}