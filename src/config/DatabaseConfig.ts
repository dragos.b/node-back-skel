export default {
    dialect: 'sqlite',
    storage: process.env.DATABASE_URL || './database.sqlite',
    logging: console.log
}